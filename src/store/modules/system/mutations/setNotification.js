/**
 * Set Application Notification
 */
const setNotification = (state, _notification) => {
    /* Set notification message. */
    state.nm = _notification
}

/* Export module. */
export default setNotification
