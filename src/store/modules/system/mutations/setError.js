/**
 * Set Application Error
 */
const SetError = (state, _error) => {
    /* Set error message. */
    state.em = _error
}

/* Export module. */
export default SetError
