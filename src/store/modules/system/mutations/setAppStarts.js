/**
 * Set Application Starts
 */
const setAppStarts = (state) => {
    /* Increment application starts. */
    state.as++
}

/* Export module. */
export default setAppStarts
