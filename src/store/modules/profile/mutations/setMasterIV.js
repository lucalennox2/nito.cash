/**
 * Set Master Initialization Vector
 */
const setMasterIV = (state, _iv) => {
    /* Set master initializatoin vector. */
    state.miv = _iv
}

/* Export module. */
export default setMasterIV
