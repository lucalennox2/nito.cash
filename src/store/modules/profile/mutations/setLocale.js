/**
 * Set Locale
 */
const setLocale = (state, _locale) => {
    /* Set locale. */
    state.l = _locale
}

/* Export module. */
export default setLocale
