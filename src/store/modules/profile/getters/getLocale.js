/**
 * Get Locale
 */
const getLocale = (state) => {
    /* Return locale. */
    return state.l
}

/* Export module. */
export default getLocale
