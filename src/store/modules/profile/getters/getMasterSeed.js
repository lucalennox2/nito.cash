/**
 * Get Master Seed
 */
const getMasterSeed = (state) => {
    /* Return master seed. */
    return state.ms
}

/* Export module. */
export default getMasterSeed
