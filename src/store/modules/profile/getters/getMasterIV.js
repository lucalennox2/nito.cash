/**
 * Get Master Initialization Vector
 */
const getMasterIV = (state) => {
    /* Return master initialization vector. */
    return state.miv
}

/* Export module. */
export default getMasterIV
