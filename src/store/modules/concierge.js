/* Import libraries. */
// import telr from '../../api/telr'

/* Import modules (getters). */
import getParsedInfo from './concierge/getters/getParsedInfo'

/* Import modules (actions). */
// ...

/* Import modules (mutations). */
// ...

/* Initialize state. */
const state = {
    //
}

/* Getters. */
const getters = {
    getParsedInfo,
}

/* Actions. */
const actions = {
    //
}

/* Mutations. */
const mutations = {
    //
}

/* Export. */
// NOTE: We DO NOT namespace here to allow for global use of `dispatch`.
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
