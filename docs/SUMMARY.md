# Summary

* [Welcome]()

    * [0.1. Readme](README.md)
    * [0.2. Foreword](FOREWORD.md)
    * [0.3. Changelog](CHANGELOG.md)
    * [0.4. Roadmap](ROADMAP.md)
    * [0.5. Contributing](CONTRIBUTING.md)

* [1. Your Crypto Wallets]()

    * [1.1. Cash Wallet]()

        * [1.1.1. Introduction](wallets/cash/intro.md)
        * [1.1.2. Where to buy BCH?]()
        * [1.1.3. Where to spend BCH?]()
        * [1.1.4. Single Ledger Protocol](wallets/cash/slp.md)
        * [1.1.5. Donations](wallets/cash/donations.md)

    * [1.2. Nito Wallet]()

        * [1.2.1. Introduction](wallets/nito/intro.md)
        * [1.2.2. Protocol](wallets/nito/protocol.md)
        * [1.2.3. Pools](wallets/nito/pools.md)

    * [1.3. Savings Wallet]()

        * [1.3.1. Introduction](wallets/savings/intro.md)
        * [1.3.2. Stablecoins]()
        * [1.3.3. Decentralized Finance (DeFi)]()
        * [1.3.4. Compound Finance]()

* [2. Nito Concierge]()

    * [2.1. Introduction](concierge/intro.md)
    * [2.2. Decentralized Finance](concierge/defi.md)
    * [2.2. Liquidity Pools](concierge/pools.md)

* [3. Nito Security]()

    * [3.1. Private Keys]()
    * [3.2. Seed Phrases]()
    * [3.3. Hardware wallets](security/hardware.md)
    * [3.4. Cookies](security/cookies.md)

* [4. Legal]()

    * [4.1. Earning Interest](legal/interest.md)
    * [4.2. Code of Conduct]()
    * [4.3. Terms of Use]()
    * [4.4. Privacy Notice]()
    * [4.5. Cookie Notice]()

* [5. Appendices]()

    * [5.1. Recommended Resources]()
