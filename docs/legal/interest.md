# Earning Interest

Interest rates are variable and changes frequently based on supply and demand in the underlying digital dollar (USDC) lending liquidity pool on Compound protocol.

\* Savings is your personal savings in digital cash (USDC).

Digital cash (USDC) is a digital representation of U.S. dollar on the Ethereum blockchain. You can read more about it here.

Each USDC is 100% backed by U.S. dollar reserves ($1 per 1 USDC) held in bank accounts of the issuer.

Issuers of USDC regularly report their U.S. dollar reserves.

Coinbase and Circle are the two leading licensed financial institutions in the U.S. that issue USDCs.

Monthly __[audit reports](https://www.centre.io/usdc-transparency)__ by a major auditing firm are publicly available.

USDC is always $1.

Nito Cash DOES NOT set or control interest rates.

Interest rates displayed on the nito.cash website and in the mobile app are in real-time.

The currently displayed interest rate may be lower or higher than it was stated previously.

Nito Cash is an app and software solution that allows users to supply funds into the Compound digital dollar liquidity pool (USDC pool).

Nito Cash does not require any financial services related license for offering the services on Nito wallet.

We only provide you a technological solution – Nito wallet – allowing you to easily interact with Compound protocol and earn interest on USDC digital cash. Nito Cash is a software and service developed by Nito Cash.

Nito Cash is not a bank, not a cryptocurrency exchange, not a custodian wallet provider and digital cash (USDC) will not be stored at a bank.

Funds supplied to Compound liquidity pools using Nito software are not insured, protected or in any way secured by any state authority (e.g government or Central Bank)

Supplying funds to the Compound lending liquidity pool using Nito Cash involves risk and you may lose money by accessing these pools through the Nito Cash.

Historical interest rates on supplying digital assets (digital dollar, stablecoin, USDC) to the Compound protocol is not an indicator that these rates will be available in the future.

Although the Compound liquidity pool that the Nito wallet provides access to is overcollateralized by digital assets (cryptocurrency) and monitored by third parties for liquidations in real-time, neither Nito Cash, nor the Compound protocol can guarantee it will be monitored in the future.

Compound Labs, Inc., the developer of Compound liquidity pools, has no legal obligation to provide software development support to its liquidity pools, and you should not expect that Compound Labs, Inc. will provide software development to its liquidity pools at any time.

It is possible that Compound liquidity pools will not have enough liquidity at the time of withdrawal if the borrowing or withdrawal demand becomes high or for any other reasons.

Digital currency (cryptocurrency, virtual currency, stablecoins) is not legal tender, is not backed by the government, and Nito Cash is not subject to any protection.

Nito Cash activities do not fall under the framework of regulated financial services and we are not supervised by the Bank of Lithuania or any other financial services supervisory authority.

You act solely at your own discretion and bear all the responsibility and risk for any and all decisions made while using the Nito Cash.
