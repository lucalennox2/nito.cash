# Welcome to the Documentation<br>for Nito Cash

![Nito.cash Poster](https://nito.cash/poster.jpg?1586230039)

[![Build Status](https://travis-ci.com/modenero/nito-cash.svg?branch=master)](https://travis-ci.com/modenero/nito-cash)
[![Greenkeeper badge](https://badges.greenkeeper.io/modenero/nito-cash.svg)](https://greenkeeper.io/)

### https://nito.cash

Documentation — __https://docs.nito.cash__

Nito.cash is the most user-friendly, privacy protective wallet for crypto newcomers.
