# Savings Wallet

Nito Savings is a savings account alternative that allows you to earn a high-interest return on your digital cash. While traditional banks pay only around __0.05%__ interest on average, a Nito Savings account can earn you up to __6%.__ There is no middleman, nor any fees involved and you can withdraw your funds with all your earned interest anytime.

## Stablecoins

...

## Decentralized Finance (DeFi)

Earn interest on your crypto.

...

## Compound Finance

https://compound.finance

...
