# Donations

![Make a change](https://i.imgur.com/sFzsSC3.png)

__https://donations.cash__

__When it comes to strength in numbers, the Bitcoin community is second to none. Throughout its history, its members have always come together to make miracles happen for those in need.__

- BitGive
- ...
- Flipstarter

![Flipstarter](https://i.imgur.com/cIPajeL.png)

Since the community's development of __[Flipstarter,](https://flipstarter.cash)__ it is now possible to donate to various public and private fundraisers, through the use of __[assurance contracts.](https://en.wikipedia.org/wiki/Assurance_contract)__

Using an assurance contract allows you to remain in __FULL control over your funds,__ until the campaign goals have been reached or the campaign time expires.

During the time of the campaign, your donation / pledge will remain "locked or frozen" for the duration.

At any point, you can "unlock" your funds and spend them as your wish.
