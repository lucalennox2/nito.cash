# Nito Wallet Introduction

__[Nito Pools](pools.md)__ allow you to choose a "trusted" network to hold your Nito Cash. __[Bitcoin Cash Please SipNET]()__ is the "official" trustless network that currently manages the pools.


# Generations

Each generation aims to provide additional decentralization through:

1. Additional participants
2. Improved protocol / security

### Agricole

_Launched: TBD_

### Name proposals

- Agricole | Ally
- Barclays | Belfius
- Caixa | Capital One | Commerz
- Danske | Dominion | Deutsche | Dexia
- Everbright
- Fubon | Fifth Third
- Goldman
- Hua Xia | Hana
- Imperial | Itaú
- JPMorgan
- KB Financial
- Lloyds
- Mutuel | Morgan | Mellon
- Nordea | Nomura
- Oversea
- Ping An
- Qatar National
- Resona
- Santander | Sumitomo | Suntrust | State Street
- Truist
- UniCredit
- Valley National
- Wells | Westpac | Woori
- X
- Y
- Zions
