# Liquidity Pools

### Earn fees

Anyone can start earning trading fees on exchange platforms like Uniswap today by being a liquidity provider. Pools fully distribute trading fees to liquidity providers.

### Own the pool

As a liquidity provider, you add a specific ratio of assets to help facilitate trades in the pool. Doing so gives you an ownership share of the pool and the future trading fees it generates.

### Stay liquid

You can pack up and leave anytime. Your ownership shares are completely liquid. Trade them in for your share of the underlying assets in the pool.
