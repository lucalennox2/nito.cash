# Decentralized Finance (DeFi)

[![Decentralized Finance](https://i.imgur.com/XGftt9e.png)](https://www.abra.com/blog/decentralized-finance-an-emerging-alternative-to-the-global-financial-system/)
_source: [Abra and Visual Capitalist](https://www.abra.com/blog/decentralized-finance-an-emerging-alternative-to-the-global-financial-system/)_

For years, limited access, inherent inefficiencies and a highly illiquid marketplace have prevented investors and issuers from unlocking the full value of the $9.5 trillion alternative asset market.

Nito Concierge is providing a unique opportunity to liquidity and transparency in the alternative asset space.
